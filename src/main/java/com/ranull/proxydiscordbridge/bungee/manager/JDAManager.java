package com.ranull.proxydiscordbridge.bungee.manager;

import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import com.ranull.proxydiscordbridge.bungee.listener.MessageReceivedListener;
import com.ranull.proxydiscordbridge.bungee.listener.ReadyListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JDAManager {
    private final ProxyDiscordBridge plugin;
    private final MessageReceivedListener messageReceivedListener;
    private final ReadyListener readyListener;
    private final Map<Long, TextChannel> textChannelMap;
    private Guild guild;
    private JDA jda;

    public JDAManager(ProxyDiscordBridge plugin) {
        this.plugin = plugin;
        this.messageReceivedListener = new MessageReceivedListener(plugin);
        this.readyListener = new ReadyListener(plugin);
        this.textChannelMap = new HashMap<>();

        reload();
    }

    public void reload() {
        shutdown();
        start();
    }

    public void start() {
        try {
            JDABuilder jdaBuilder = JDABuilder.createDefault(plugin.getConfig().getString("bot.token"));

            jdaBuilder.disableCache(Arrays.asList(CacheFlag.values()));
            jdaBuilder.addEventListeners(messageReceivedListener, readyListener);

            jda = jdaBuilder.build();
        } catch (LoginException exception) {
            plugin.getLogger().info(exception.getMessage());
        }
    }

    public void shutdown() {
        if (jda != null) {
            jda.removeEventListener(messageReceivedListener, readyListener);
        }
    }

    public void sendMessage(String message, long channel) {
        if (textChannelMap.containsKey(channel)) {
            textChannelMap.get(channel).sendMessage(message).allowedMentions(Collections.emptyList()).queue();
        }
    }

    public void reloadCache() {
        reloadGuildCache();
        reloadChannelCache();
    }

    private void reloadGuildCache() {
        long id = plugin.getConfig().getLong("bot.server", 0);

        if (id != 0) {
            guild = jda.getGuildById(id);
        }
    }

    private void reloadChannelCache() {
        textChannelMap.clear();

        if (guild != null) {
            for (String key : plugin.getConfig().getSection("groups").getKeys()) {
                long id = plugin.getConfig().getLong("groups." + key);

                if (id != 0) {
                    TextChannel textChannel = guild.getTextChannelById(id);

                    if (textChannel != null) {
                        textChannelMap.put(id, textChannel);
                    }
                }
            }
        }
    }
}
