package com.ranull.proxydiscordbridge.bungee.manager;

import com.ranull.proxychatbridge.bungee.ProxyChatBridge;
import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import net.md_5.bungee.api.ChatColor;

public class ChatManager {
    private final ProxyDiscordBridge plugin;

    public ChatManager(ProxyDiscordBridge plugin) {
        this.plugin = plugin;
    }

    public void sendMessageMinecraft(String name, String message, long channel) {
        if (plugin.getConfig().contains("channels." + channel)) {
            String group = plugin.getConfig().getString("channels." + channel, "global");
            String source = "discord:" + channel;
            String format = plugin.getConfig().getString("format.minecraft")
                    .replace("%group%", group)
                    .replace("%source%", source)
                    .replace("%channel%", channel + "")
                    .replace("%name%", "%1$s")
                    .replace("%message%", "%2$s")
                    .replace("&", "§");

            ProxyChatBridge.sendMessage(name, format, message, group, source);
        }
    }

    public void sendMessageDiscord(String name, String message, String group, String source) {
        if (plugin.hasJDAManager()) {
            long channel = plugin.getConfig().getLong("groups." + group, 0);

            if (channel != 0) {
                message = plugin.getConfig().getString("format.discord")
                        .replace("%group%", group)
                        .replace("%name%", name)
                        .replace("%message%", message);

                if (source != null && message.contains("%source%")) {
                    message = message.replace("%source%", source);
                } else {
                    message = message.replace("%source%", "");
                }

                plugin.getJDAManager().sendMessage(ChatColor.stripColor(message), channel);
            }
        }
    }
}