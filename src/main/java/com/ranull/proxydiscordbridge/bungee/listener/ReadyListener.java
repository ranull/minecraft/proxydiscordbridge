package com.ranull.proxydiscordbridge.bungee.listener;

import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class ReadyListener extends ListenerAdapter {
    private final ProxyDiscordBridge plugin;

    public ReadyListener(ProxyDiscordBridge plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        plugin.getJDAManager().reloadCache();
    }
}
