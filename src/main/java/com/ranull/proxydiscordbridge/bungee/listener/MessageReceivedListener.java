package com.ranull.proxydiscordbridge.bungee.listener;

import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class MessageReceivedListener extends ListenerAdapter {
    private final ProxyDiscordBridge plugin;

    public MessageReceivedListener(ProxyDiscordBridge plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getAuthor().isBot()) {
            plugin.getChatManager().sendMessageMinecraft(event.getMessage().getAuthor().getName(),
                    event.getMessage().getContentStripped(), event.getChannel().getIdLong());
        }
    }
}
