package com.ranull.proxydiscordbridge.bungee.listener;

import com.ranull.proxychatbridge.bungee.event.ExternalChatReceiveEvent;
import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class ExternalChatListener implements Listener {
    private final ProxyDiscordBridge plugin;

    public ExternalChatListener(ProxyDiscordBridge plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExternalChat(ExternalChatReceiveEvent event) {
        if (!event.isCancelled()) {
            plugin.getChatManager().sendMessageDiscord(event.getName(), event.getMessage(), event.getGroup(),
                    event.getSource());
        }
    }
}
