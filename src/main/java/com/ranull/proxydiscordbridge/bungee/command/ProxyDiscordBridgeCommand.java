package com.ranull.proxydiscordbridge.bungee.command;

import com.ranull.proxydiscordbridge.bungee.ProxyDiscordBridge;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class ProxyDiscordBridgeCommand extends Command {
    private final ProxyDiscordBridge plugin;

    public ProxyDiscordBridgeCommand(ProxyDiscordBridge plugin) {
        super("proxydiscordbridge", null, "bungeediscordbridge", "velocitydiscordbridge", "pdb", "bdb", "vdb");
        this.plugin = plugin;
    }

    public void execute(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(ChatColor.BLUE + "✉" + ChatColor.DARK_GRAY + " » " + ChatColor.BLUE
                    + "ProxyDiscordBridge " + ChatColor.DARK_GRAY + "v" + plugin.getDescription().getVersion());
            commandSender.sendMessage(
                    ChatColor.GRAY + "/pdb " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");

            if (commandSender.hasPermission("proxydiscordbridge.reload")) {
                commandSender.sendMessage(ChatColor.GRAY + "/pdb reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }

            commandSender.sendMessage(ChatColor.BLUE + "Author: " + ChatColor.GRAY + "Ranull");
        } else if (args[0].equals("reload")) {
            if (commandSender.hasPermission("proxydiscordbridge.reload")) {
                plugin.reloadConfig();
                plugin.getJDAManager().reload();
                commandSender.sendMessage(ChatColor.BLUE + "✉" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "Reloaded config file.");
            } else {
                commandSender.sendMessage(ChatColor.BLUE + "✉" + ChatColor.DARK_GRAY + " » "
                        + ChatColor.RESET + "No permission.");
            }
        }
    }
}
