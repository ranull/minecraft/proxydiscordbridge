package com.ranull.proxydiscordbridge.bungee;

import com.ranull.proxydiscordbridge.bungee.command.ProxyDiscordBridgeCommand;
import com.ranull.proxydiscordbridge.bungee.listener.ExternalChatListener;
import com.ranull.proxydiscordbridge.bungee.manager.ChatManager;
import com.ranull.proxydiscordbridge.bungee.manager.ConfigManager;
import com.ranull.proxydiscordbridge.bungee.manager.JDAManager;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import org.bstats.bungeecord.MetricsLite;

public class ProxyDiscordBridge extends Plugin {
    private ConfigManager configManager;
    private ChatManager chatManager;
    private JDAManager jdaManager;

    @Override
    public void onEnable() {
        configManager = new ConfigManager(this);
        chatManager = new ChatManager(this);
        jdaManager = new JDAManager(this);

        registerMetrics();
        registerListeners();
        registerCommands();
    }

    @Override
    public void onDisable() {
        if (jdaManager != null) {
            jdaManager.shutdown();
        }
    }

    private void registerMetrics() {
        new MetricsLite(this, 17246);
    }

    private void registerListeners() {
        getProxy().getPluginManager().registerListener(this, new ExternalChatListener(this));
    }

    private void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new ProxyDiscordBridgeCommand(this));
    }

    public ChatManager getChatManager() {
        return chatManager;
    }

    public JDAManager getJDAManager() {
        return jdaManager;
    }

    public boolean hasJDAManager() {
        return jdaManager != null;
    }

    public Configuration getConfig() {
        return configManager.getConfig();
    }

    public void reloadConfig() {
        configManager.loadConfig();
    }
}
