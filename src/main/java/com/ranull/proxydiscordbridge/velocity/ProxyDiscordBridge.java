package com.ranull.proxydiscordbridge.velocity;

import com.google.inject.Inject;
import com.velocitypowered.api.proxy.ProxyServer;

import java.util.logging.Logger;

public class ProxyDiscordBridge {
    private final ProxyServer proxyServer;
    private final Logger logger;

    @Inject
    public ProxyDiscordBridge(ProxyServer proxyServer, Logger logger) {
        this.proxyServer = proxyServer;
        this.logger = logger;

        logger.info("ProxyDiscordBridge Velocity support not implemented.");
    }

    public ProxyServer getProxyServer() {
        return proxyServer;
    }

    public Logger getLogger() {
        return logger;
    }
}